# nexial-docker

(Unofficial) Docker images for nexial automation utility

Docker hub: https://hub.docker.com/u/digitalfreedom/nexial-core
Nexial Repository: https://github.com/nexiality/nexial-core

## Building the image:

```sh
docker build -t nexial-core .
```

## Building alpine image:

```sh
docker build -t nexial-core:alpine - < Dockerfile.alpine
```
