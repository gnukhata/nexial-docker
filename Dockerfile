# Author: Sai Karthik <kskarthik@disroot.org>

FROM debian:sid-slim

ARG NEXIAL_VER=4.5_1534

ARG CHROME_DRIVER_VER=126.0.6478.182

RUN apt update && apt install -y openjdk-19-jdk chromium chromium-driver wget unzip

RUN adduser --disabled-password nexial-user

USER nexial-user

WORKDIR /home/nexial-user/

RUN wget -O nexial.zip https://github.com/nexiality/nexial-core/releases/download/nexial-core-v$NEXIAL_VER/nexial-core-$NEXIAL_VER.zip \
		&& unzip -q nexial.zip -d nexial-core \
		&& rm nexial.zip

RUN wget -O chrome_driver.zip https://storage.googleapis.com/chrome-for-testing-public/$CHROME_DRIVER_VER/linux64/chromedriver-linux64.zip \
        && unzip -q chrome_driver.zip -d . \
		&& mkdir -p /home/nexial-user/.nexial/chrome/ \
		&& mv -v chromedriver-linux64/* /home/nexial-user/.nexial/chrome/ \
		&& rm chrome_driver.zip \
		&& rmdir chromedriver-linux64

RUN echo '{"driverVersion": "126.0.6478", "lastChecked": 1720627880898, "downloadAgent": "nexial-4.5_1534", "neverCheck": true, "compatibleDriverVersion": "126.0.6478.182"}' >> /home/nexial-user/.nexial/chrome/.manifest

ENV NEXIAL_HOME=/home/nexial-user/nexial-core \
		PATH=/home/nexial-user/nexial-core/bin:$PATH \
		FIREFOX_BIN=/usr/bin/firefox \
		CHROME_BIN=/usr/bin/chromium \
		JAVA_OPT="$JAVA_OPT -Dnexial.browser=chrome.headless"

#TODO: fix tests failing with firefox headless mode
# JAVA_OPT=$JAVA_OPT -Dnexial.browser=chrome.headless -Dnexial.browser=firefox.headless

CMD [ "/bin/bash" ]

# uncomment for troubleshooting
# ENTRYPOINT [ "tail", "-F", "/dev/null" ]
